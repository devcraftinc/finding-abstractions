﻿// Copyright 2018-2018 Hexagon Software LLC
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;

namespace ByTestability
{
  public class GameBoard
  {
    private readonly Random random;

    private bool player1Turn = true;

    public GameBoard(Random random)
    {
      this.random = random;
    }

    public int Player1Score { get; set; }
    public int Player2Score { get; set; }

    public int Piece1X { get; set; }
    public int Piece1Y { get; set; }

    public int Piece2X { get; set; }
    public int Piece2Y { get; set; }

    public void PlayTurn()
    {
      Console.WriteLine("It's Player " + (player1Turn ? "1" : "2") + "'s turn.");
      var distance = random.Next(6) + 1;
      distance += random.Next(6) + 1;
      distance += random.Next(6) + 1;

      for (var i = 0; i < distance; ++i)
        Hop();

      player1Turn = !player1Turn;
    }

    private void Hop()
    {
      var x = player1Turn ? Piece1X : Piece2X;
      var y = player1Turn ? Piece1Y : Piece2Y;

      if (x == 0)
      {
        if (y < 9)
          y++;
        else
          x++;
      }
      else if (x == 9)
      {
        if (y > 0)
          y--;
        else
          x--;
      }
      else
      {
        if (y == 9)
          x++;
        else
          x--;
      }

      var score = (x == 0 && y == 0);

      Console.WriteLine("(" + x + ", " + y + ")" + (score ? "...SCORE!" : ""));

      if (player1Turn)
      {
        Piece1X = x;
        Piece1Y = y;
        if (score)
          Player1Score++;
      }
      else
      {
        Piece2X = x;
        Piece2Y = y;
        if (score)
          Player2Score++;
      }
    }
  }
}