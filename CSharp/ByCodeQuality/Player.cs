﻿// Copyright 2018-2018 Hexagon Software LLC
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

namespace ByCodeQuality
{
  class Player : GameObject
  {
    public int Hearts { get; set; }
    public int Shields { get; set; }
    public int Arrows { get; set; }

    public void CheckForPickUp(GameObject[] nearby)
    {
      foreach (var candidate in nearby)
      {
        if (!(candidate is PickUp)) continue;
        if (candidate.X + 16 < X - 32) continue;
        if (candidate.Y + 16 < Y - 92) continue;
        if (candidate.X - 16 > X + 24) continue;
        if (candidate.Y - 16 > Y + 48) continue;

        var pickup = (PickUp) candidate;
        Hearts += pickup.Hearts;
        Shields += pickup.Shields;
        Arrows += pickup.Arrows;

        pickup.Destroy();
      }
    }
  }
}