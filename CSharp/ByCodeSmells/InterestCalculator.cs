﻿// Copyright 2018-2018 Hexagon Software LLC
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;

namespace ByCodeSmells
{
  class InterestCalculator
  {
    public static long UpfrontForMonths(long pennies, double rate, int months)
    {
      var factor = Math.Pow(1 + rate, months / 12d);
      return (long) Math.Round(pennies * factor);
    }

    public static long MonthlyCompound(long pennies, double rate, int months)
    {
      var monthly = Math.Pow(1 + rate, 1d / 12);
      for (var i = 0; i < months; ++i)
        pennies = (long) Math.Round(pennies * monthly);

      return pennies;
    }

    public static long YearlyCompound(long pennies, double rate, double years)
    {
      var factor = Math.Pow(1 + rate, years);

      return (long) Math.Round(pennies * factor);
    }

    public static long DailySimple(long pennies, double rate, int days)
    {
      var daily = Math.Pow(1 + rate, 1d / 365) * pennies;
      pennies += (long) Math.Round(daily * days * pennies);

      return pennies;
    }
  }
}