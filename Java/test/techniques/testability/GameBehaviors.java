// Copyright 2018-2018 Hexagon Software LLC
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package techniques.testability;


import org.junit.*;

import java.io.*;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class GameBehaviors {
    private ByteArrayOutputStream consoleBytes;

    @Before
    public void setup() {
        consoleBytes = new ByteArrayOutputStream();
        System.setOut(new PrintStream(consoleBytes));
    }

    @Test
    public void journey1() {
        testCase(4914017, 1, 0, 0, 3, 9, 0, 0, "It's Player 1's turn.\r\n" +
                "(0, 1)\r\n" +
                "(0, 2)\r\n" +
                "(0, 3)\r\n" +
                "(0, 4)\r\n" +
                "(0, 5)\r\n" +
                "(0, 6)\r\n" +
                "(0, 7)\r\n" +
                "(0, 8)\r\n" +
                "(0, 9)\r\n" +
                "(1, 9)\r\n" +
                "(2, 9)\r\n" +
                "(3, 9)\r\n");
    }

    @Test
    public void journey2() {
        testCase(4914017, 4, 0, 0, 9, 4, 7, 9, "It's Player 1's turn.\r\n" +
                "(0, 1)\r\n" +
                "(0, 2)\r\n" +
                "(0, 3)\r\n" +
                "(0, 4)\r\n" +
                "(0, 5)\r\n" +
                "(0, 6)\r\n" +
                "(0, 7)\r\n" +
                "(0, 8)\r\n" +
                "(0, 9)\r\n" +
                "(1, 9)\r\n" +
                "(2, 9)\r\n" +
                "(3, 9)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(0, 1)\r\n" +
                "(0, 2)\r\n" +
                "(0, 3)\r\n" +
                "(0, 4)\r\n" +
                "(0, 5)\r\n" +
                "(0, 6)\r\n" +
                "(0, 7)\r\n" +
                "(0, 8)\r\n" +
                "It's Player 1's turn.\r\n" +
                "(4, 9)\r\n" +
                "(5, 9)\r\n" +
                "(6, 9)\r\n" +
                "(7, 9)\r\n" +
                "(8, 9)\r\n" +
                "(9, 9)\r\n" +
                "(9, 8)\r\n" +
                "(9, 7)\r\n" +
                "(9, 6)\r\n" +
                "(9, 5)\r\n" +
                "(9, 4)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(0, 9)\r\n" +
                "(1, 9)\r\n" +
                "(2, 9)\r\n" +
                "(3, 9)\r\n" +
                "(4, 9)\r\n" +
                "(5, 9)\r\n" +
                "(6, 9)\r\n" +
                "(7, 9)\r\n");
    }

    @Test
    public void journey3() {
        testCase(4914017, 11, 1, 1, 9, 4, 7, 9, "It's Player 1's turn.\r\n" +
                "(0, 1)\r\n" +
                "(0, 2)\r\n" +
                "(0, 3)\r\n" +
                "(0, 4)\r\n" +
                "(0, 5)\r\n" +
                "(0, 6)\r\n" +
                "(0, 7)\r\n" +
                "(0, 8)\r\n" +
                "(0, 9)\r\n" +
                "(1, 9)\r\n" +
                "(2, 9)\r\n" +
                "(3, 9)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(0, 1)\r\n" +
                "(0, 2)\r\n" +
                "(0, 3)\r\n" +
                "(0, 4)\r\n" +
                "(0, 5)\r\n" +
                "(0, 6)\r\n" +
                "(0, 7)\r\n" +
                "(0, 8)\r\n" +
                "It's Player 1's turn.\r\n" +
                "(4, 9)\r\n" +
                "(5, 9)\r\n" +
                "(6, 9)\r\n" +
                "(7, 9)\r\n" +
                "(8, 9)\r\n" +
                "(9, 9)\r\n" +
                "(9, 8)\r\n" +
                "(9, 7)\r\n" +
                "(9, 6)\r\n" +
                "(9, 5)\r\n" +
                "(9, 4)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(0, 9)\r\n" +
                "(1, 9)\r\n" +
                "(2, 9)\r\n" +
                "(3, 9)\r\n" +
                "(4, 9)\r\n" +
                "(5, 9)\r\n" +
                "(6, 9)\r\n" +
                "(7, 9)\r\n" +
                "It's Player 1's turn.\r\n" +
                "(9, 3)\r\n" +
                "(9, 2)\r\n" +
                "(9, 1)\r\n" +
                "(9, 0)\r\n" +
                "(8, 0)\r\n" +
                "(7, 0)\r\n" +
                "(6, 0)\r\n" +
                "(5, 0)\r\n" +
                "(4, 0)\r\n" +
                "(3, 0)\r\n" +
                "(2, 0)\r\n" +
                "(1, 0)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(8, 9)\r\n" +
                "(9, 9)\r\n" +
                "(9, 8)\r\n" +
                "(9, 7)\r\n" +
                "(9, 6)\r\n" +
                "(9, 5)\r\n" +
                "(9, 4)\r\n" +
                "(9, 3)\r\n" +
                "(9, 2)\r\n" +
                "(9, 1)\r\n" +
                "(9, 0)\r\n" +
                "(8, 0)\r\n" +
                "(7, 0)\r\n" +
                "(6, 0)\r\n" +
                "(5, 0)\r\n" +
                "(4, 0)\r\n" +
                "(3, 0)\r\n" +
                "It's Player 1's turn.\r\n" +
                "(0, 0)...SCORE!\r\n" +
                "(0, 1)\r\n" +
                "(0, 2)\r\n" +
                "(0, 3)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(2, 0)\r\n" +
                "(1, 0)\r\n" +
                "(0, 0)...SCORE!\r\n" +
                "(0, 1)\r\n" +
                "(0, 2)\r\n" +
                "(0, 3)\r\n" +
                "(0, 4)\r\n" +
                "It's Player 1's turn.\r\n" +
                "(0, 4)\r\n" +
                "(0, 5)\r\n" +
                "(0, 6)\r\n" +
                "(0, 7)\r\n" +
                "(0, 8)\r\n" +
                "(0, 9)\r\n" +
                "(1, 9)\r\n" +
                "(2, 9)\r\n" +
                "(3, 9)\r\n" +
                "(4, 9)\r\n" +
                "(5, 9)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(0, 5)\r\n" +
                "(0, 6)\r\n" +
                "(0, 7)\r\n" +
                "(0, 8)\r\n" +
                "(0, 9)\r\n" +
                "(1, 9)\r\n" +
                "(2, 9)\r\n" +
                "(3, 9)\r\n" +
                "(4, 9)\r\n" +
                "(5, 9)\r\n" +
                "(6, 9)\r\n" +
                "(7, 9)\r\n" +
                "It's Player 1's turn.\r\n" +
                "(6, 9)\r\n" +
                "(7, 9)\r\n" +
                "(8, 9)\r\n" +
                "(9, 9)\r\n" +
                "(9, 8)\r\n" +
                "(9, 7)\r\n" +
                "(9, 6)\r\n" +
                "(9, 5)\r\n" +
                "(9, 4)\r\n");
    }

    @Test
    public void journey4() {
        testCase(251, 12, 1, 1, 9, 0, 9, 1, "It's Player 1's turn.\r\n" +
                "(0, 1)\r\n" +
                "(0, 2)\r\n" +
                "(0, 3)\r\n" +
                "(0, 4)\r\n" +
                "(0, 5)\r\n" +
                "(0, 6)\r\n" +
                "(0, 7)\r\n" +
                "(0, 8)\r\n" +
                "(0, 9)\r\n" +
                "(1, 9)\r\n" +
                "(2, 9)\r\n" +
                "(3, 9)\r\n" +
                "(4, 9)\r\n" +
                "(5, 9)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(0, 1)\r\n" +
                "(0, 2)\r\n" +
                "(0, 3)\r\n" +
                "(0, 4)\r\n" +
                "(0, 5)\r\n" +
                "(0, 6)\r\n" +
                "(0, 7)\r\n" +
                "(0, 8)\r\n" +
                "(0, 9)\r\n" +
                "(1, 9)\r\n" +
                "(2, 9)\r\n" +
                "It's Player 1's turn.\r\n" +
                "(6, 9)\r\n" +
                "(7, 9)\r\n" +
                "(8, 9)\r\n" +
                "(9, 9)\r\n" +
                "(9, 8)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(3, 9)\r\n" +
                "(4, 9)\r\n" +
                "(5, 9)\r\n" +
                "(6, 9)\r\n" +
                "(7, 9)\r\n" +
                "(8, 9)\r\n" +
                "(9, 9)\r\n" +
                "(9, 8)\r\n" +
                "(9, 7)\r\n" +
                "(9, 6)\r\n" +
                "It's Player 1's turn.\r\n" +
                "(9, 7)\r\n" +
                "(9, 6)\r\n" +
                "(9, 5)\r\n" +
                "(9, 4)\r\n" +
                "(9, 3)\r\n" +
                "(9, 2)\r\n" +
                "(9, 1)\r\n" +
                "(9, 0)\r\n" +
                "(8, 0)\r\n" +
                "(7, 0)\r\n" +
                "(6, 0)\r\n" +
                "(5, 0)\r\n" +
                "(4, 0)\r\n" +
                "(3, 0)\r\n" +
                "(2, 0)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(9, 5)\r\n" +
                "(9, 4)\r\n" +
                "(9, 3)\r\n" +
                "(9, 2)\r\n" +
                "(9, 1)\r\n" +
                "(9, 0)\r\n" +
                "(8, 0)\r\n" +
                "(7, 0)\r\n" +
                "(6, 0)\r\n" +
                "(5, 0)\r\n" +
                "(4, 0)\r\n" +
                "(3, 0)\r\n" +
                "(2, 0)\r\n" +
                "(1, 0)\r\n" +
                "It's Player 1's turn.\r\n" +
                "(1, 0)\r\n" +
                "(0, 0)...SCORE!\r\n" +
                "(0, 1)\r\n" +
                "(0, 2)\r\n" +
                "(0, 3)\r\n" +
                "(0, 4)\r\n" +
                "(0, 5)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(0, 0)...SCORE!\r\n" +
                "(0, 1)\r\n" +
                "(0, 2)\r\n" +
                "(0, 3)\r\n" +
                "(0, 4)\r\n" +
                "(0, 5)\r\n" +
                "It's Player 1's turn.\r\n" +
                "(0, 6)\r\n" +
                "(0, 7)\r\n" +
                "(0, 8)\r\n" +
                "(0, 9)\r\n" +
                "(1, 9)\r\n" +
                "(2, 9)\r\n" +
                "(3, 9)\r\n" +
                "(4, 9)\r\n" +
                "(5, 9)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(0, 6)\r\n" +
                "(0, 7)\r\n" +
                "(0, 8)\r\n" +
                "(0, 9)\r\n" +
                "(1, 9)\r\n" +
                "(2, 9)\r\n" +
                "(3, 9)\r\n" +
                "It's Player 1's turn.\r\n" +
                "(6, 9)\r\n" +
                "(7, 9)\r\n" +
                "(8, 9)\r\n" +
                "(9, 9)\r\n" +
                "(9, 8)\r\n" +
                "(9, 7)\r\n" +
                "(9, 6)\r\n" +
                "(9, 5)\r\n" +
                "(9, 4)\r\n" +
                "(9, 3)\r\n" +
                "(9, 2)\r\n" +
                "(9, 1)\r\n" +
                "(9, 0)\r\n" +
                "It's Player 2's turn.\r\n" +
                "(4, 9)\r\n" +
                "(5, 9)\r\n" +
                "(6, 9)\r\n" +
                "(7, 9)\r\n" +
                "(8, 9)\r\n" +
                "(9, 9)\r\n" +
                "(9, 8)\r\n" +
                "(9, 7)\r\n" +
                "(9, 6)\r\n" +
                "(9, 5)\r\n" +
                "(9, 4)\r\n" +
                "(9, 3)\r\n" +
                "(9, 2)\r\n" +
                "(9, 1)\r\n");
    }

    private void testCase(int seed, int count, int expectedPlayer1Score, int expectedPlayer2Score, int expectedPiece1X, int expectedPiece1Y, int expectedPiece2X, int expectedPiece2Y, String expectedConsoleOutput) {
        GameBoard board = new GameBoard(new Random(seed));

        for (int i = 0; i < count; ++i)
            board.playTurn();

        assertEquals(expectedPlayer1Score, board.getPlayer1Score());
        assertEquals(expectedPlayer2Score, board.getPlayer2Score());
        assertEquals(expectedPiece1X, board.getPiece1X());
        assertEquals(expectedPiece1Y, board.getPiece1Y());
        assertEquals(expectedPiece2X, board.getPiece2X());
        assertEquals(expectedPiece2Y, board.getPiece2Y());
        assertEquals(expectedConsoleOutput, new String(consoleBytes.toByteArray()));
    }
}

