// Copyright 2018-2018 Hexagon Software LLC
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package techniques.quality;

class Player extends GameObject {
    private int hearts;

    public int getHearts() {
        return hearts;
    }

    public void setHearts(int value) {
        hearts = value;
    }

    private int shields;

    public int getShields() {
        return shields;
    }

    public void setShields(int value) {
        shields = value;
    }

    private int arrows;

    public int getArrows() {
        return arrows;
    }

    public void setArrows(int value) {
        arrows = value;
    }

    public void checkForPickUp(GameObject[] nearby) {
        for (GameObject candidate : nearby) {
            if (!(candidate instanceof PickUp)) continue;
            if (candidate.getX() + 16 < getX() - 32) continue;
            if (candidate.getY() + 16 < getY() - 92) continue;
            if (candidate.getX() - 16 > getX() + 24) continue;
            if (candidate.getY() - 16 > getY() + 48) continue;

            PickUp pickup = (PickUp) candidate;
            setHearts(getHearts() + pickup.getHearts());
            setShields(getShields() + pickup.getShields());
            setArrows(getArrows() + pickup.getArrows());

            pickup.destroy();
        }
    }
}

