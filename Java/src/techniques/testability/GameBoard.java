// Copyright 2018-2018 Hexagon Software LLC
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package techniques.testability;

import java.util.*;

public class GameBoard {
    private final Random random;

    private boolean player1Turn = true;

    public GameBoard(Random random) {
        this.random = random;
    }

    private int player1Score;

    public int getPlayer1Score() {
        return player1Score;
    }

    public void setPlayer1Score(int value) {
        player1Score = value;
    }

    private int player2Score;

    public int getPlayer2Score() {
        return player2Score;
    }

    public void setPlayer2Score(int value) {
        player2Score = value;
    }

    private int piece1X;

    public int getPiece1X() {
        return piece1X;
    }

    public void setPiece1X(int value) {
        piece1X = value;
    }

    private int piece1Y;

    public int getPiece1Y() {
        return piece1Y;
    }

    public void setPiece1Y(int value) {
        piece1Y = value;
    }

    private int piece2X;

    public int getPiece2X() {
        return piece2X;
    }

    public void setPiece2X(int value) {
        piece2X = value;
    }

    private int piece2Y;

    public int getPiece2Y() {
        return piece2Y;
    }

    public void setPiece2Y(int value) {
        piece2Y = value;
    }

    public void playTurn() {
        System.out.println("It's Player " + (player1Turn ? "1" : "2") + "'s turn.");
        int distance = random.nextInt(6) + 1;
        distance += random.nextInt(6) + 1;
        distance += random.nextInt(6) + 1;

        for (int i = 0; i < distance; ++i)
            hop();

        player1Turn = !player1Turn;
    }

    private void hop() {
        int x = player1Turn ? getPiece1X() : getPiece2X();
        int y = player1Turn ? getPiece1Y() : getPiece2Y();

        if (x == 0) {
            if (y < 9)
                y++;
            else
                x++;
        } else if (x == 9) {
            if (y > 0)
                y--;
            else
                x--;
        } else {
            if (y == 9)
                x++;
            else
                x--;
        }

        boolean score = (x == 0 && y == 0);

        System.out.println("(" + x + ", " + y + ")" + (score ? "...SCORE!" : ""));

        if (player1Turn) {
            setPiece1X(x);
            setPiece1Y(y);
            if (score)
                setPlayer1Score(getPlayer1Score() + 1);
        } else {
            setPiece2X(x);
            setPiece2Y(y);
            if (score)
                setPlayer2Score(getPlayer2Score() + 1);
        }
    }
}

